import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router'; // בכדי לתפוס את האיידי מהראוטר
import {UsersService} from './../users.service';

@Component({
  selector: 'user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
user;
  constructor(private rout:ActivatedRoute, private service:UsersService) { //גורם לאנגולר לראות שרצינו ליצור משתנה בשם ראוט ולייצר אובייקט חדש מסוג זה ולהפוך אותו לתכונה של המחקה הזאת=דיפנדנסי אינג'קשן ,האובייקט הוא ראוט מסוג אקטיביטד ראוט
    
   }

  ngOnInit() { // הקוד מופעל כרשר הקומפוננט נוצר= גם הקונסטרקטור פועל כשהקומפוננט נוצר
    this.rout.paramMap.subscribe(params=>{ //קריאת האיידי מהיואראל

      let id = params.get('id'); // שליפת האיידי
      console.log(id);
      this.service.getUser(id).subscribe(response=>{ // יצירת אובסרבסל
        this.user = response.json();
        console.log(this.user);
      })
    }) 
  }

}