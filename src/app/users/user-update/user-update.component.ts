
import { Component, OnInit,Output , EventEmitter } from '@angular/core';
import { UsersService } from './../users.service';
import {FormGroup , FormControl} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from "@angular/router";

@Component({
  selector: 'user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit {
@Output() addUser:EventEmitter<any> = new EventEmitter<any>(); //any מגדיר סוג מסוים של מידע, במקרה הזה כל סוג
    @Output() addUserPs:EventEmitter<any> = new EventEmitter<any>(); //אירוע פסימי

      service:UsersService;
      //Reactive Form
      //מתאים בדיוק לטופס עצמו
      usrform = new FormGroup({
          name:new FormControl(),
          email:new FormControl()
      });
  constructor(private route: ActivatedRoute ,service: UsersService, private router:Router) { 
    this.service = service;
  }

    sendData() {
        //הפליטה של העדכון לאב
      this.addUser.emit(this.usrform.value.name);
      console.log(this.usrform.value);
      this.route.paramMap.subscribe(params=>{
        let id = params.get('id');
        this.service.putUser(this.usrform.value, id).subscribe(
          response => {
            console.log(response.json());
            this.addUserPs.emit();
            this.router.navigate(['/']);
          }
        );
      })
    }
    user;


  ngOnInit() {

       this.route.paramMap.subscribe(params=>{
           let id = params.get('id');
           console.log(id);
           this.service.getUser(id).subscribe(response=>{
             this.user = response.json();
             console.log(this.user);
            })
        })
   }
            
}