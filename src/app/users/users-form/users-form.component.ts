
import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import {FormGroup,FormControl,Validators, FormBuilder  } from '@angular/forms'; //הוספה
import { UsersService } from "../users.service";


@Component({
  selector: 'users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.css']
})
export class UsersFormComponent implements OnInit {
 
  @Output() addUser:EventEmitter<any> = new EventEmitter<any>(); // הגדרת משתנה חדש מסוג איוונט אמיטר - סוג המידע שיועבר לא מוגדר- התשתית שתעביר לנו את המידע מאלמנט הבן לאלמנט האב
  @Output() addUserPs:EventEmitter<any> = new EventEmitter<any>();
  service:UsersService;
  usrform = new FormGroup({ // בניית מבנה נתונים בקוד המתאים אחד לאחד לטופס ההטמל. הנתונים ישמרו כמשתנים בקוד -> זהו אובייקט שדרכו תתבצע ה"תפירה" בטופס
    name:new FormControl(),//Validators.required
    email:new FormControl(),
    password: new FormControl(),
    
  });

  sendData(){
    this.addUser.emit(this.usrform.value.name);// פליטת תוכן ההודעה שנשלחה למסג'ס (מי שמעליי בהיררכיה) - התרחש אירוע של אד מסג'ס
    console.log(this.usrform.value); //  נשלח את הנתונים שהוכנסו בטופס לקונסול
    this.service.postUser(this.usrform.value).subscribe(
      response => {
        console.log(response.json())
        this.addUserPs.emit(); // מתזמנת את האירוע בניגוד לאופטימיסטי שמעדכן את המידע
        
      }
    )
  };
  
  constructor(service:UsersService,private formBuilder:FormBuilder) {
    this.service = service;
   }

  ngOnInit() {
    this.usrform = this.formBuilder.group({
	      name:  [null, [Validators.required]],
        email: [null, Validators.required],
        password: [null, Validators.required],
	    });
  }

}