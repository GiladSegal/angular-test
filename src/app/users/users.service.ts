import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import{Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class UsersService {
   http:Http;//http -> שם התכונה. Http-> סוג התכונה
 db:AngularFireDatabase;
   
 getUsers(){
     console.log(environment.url);
      let token = localStorage.getItem('auth');//remove if problems
   console.log(token);//remove if trouble
    return this.http.get(environment.url+"users?token="+token);//remove ?token... if wrong
  }

  getUser(id){
    return this.http.get(environment.url+'user/' + id);
  }



  updateUser(id,user){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('username',user.username).append('email',user.email);
    return this.http.put(environment.url+'user/'+id, params.toString(), options);      
  }


  postUser(data) // דטה= נתונים של הטופס
  {
    let options = { // הגדרנו דרך המחלקה של אנגולר האדר , רכיב קונטנט טייפ בישביל שהנתונים יעברו
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
   let token = localStorage.getItem('auth');
   console.log(token);
    let  params = new HttpParams().append('name', data.name).append('email',data.email).append('password',data.password).append('token',token);
   console.log(data) // פאראמס הוא מבנה נתונים המחזיק מפתח וערך 
   return this.http.post(environment.url+'users', params.toString(),options);

  }

    putUser(data,key){
        let options = {
          headers: new Headers({
            'content-type':'application/x-www-form-urlencoded'
          })
   
        }
        let params = new HttpParams().append('username',data.name).append('email',data.email);
        return this.http.put(environment.url+'user/'+ key,params.toString(), options);
      }

  deleteUser(key){
    return this.http.delete(environment.url+'user/'+key);
  }

  getUsersFire(){
    return this.db.list('/users').valueChanges();
  }

  constructor(http:Http,db:AngularFireDatabase) {
    this.http = http;
    this.db=db;
   }

}