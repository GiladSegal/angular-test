import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users;
usersKeys =[];


  constructor(private service:UsersService,private router:Router) {
    service.getUsers().subscribe(
      response=>{//console.log(response)//arrow function .json() converts the string that we recived to jason
      this.users= response.json();
      this.usersKeys = Object.keys(this.users);
      console.log(this.users);
   });

   }

   optimisticAdd(user){
    // console.log("addMessage worked" + message); בדיקה בלוג שהכל עובד
 
    var newKey = parseInt(this.usersKeys[this.usersKeys.length-1],0) +1; //מניפולציה שמאפשרת את שליחת ההודעה- חייב להיות איידי להודעה ולכן אנחנו "ממציאים" מספר איידי זמני בישביל לשלוח את ההודעה
    var newUserObject = {}; // יוצרים אובייקט חדש של מסג'ס
    newUserObject['name'] = user; // יצירת אובייקט חדש של מסג'ס
    this.users[newKey] = newUserObject; // קישור ההודעה לאיידי הפקטיבי שיצרנו
     this.usersKeys = Object.keys(this.users); //הוספת האיבר האחרון של מסג'סקיס
   }
 
   pessimisticAdd(){
 
       this.service.getUsers().subscribe(response=>{
       console.log(response)//arrow function .json() converts the string that we recived to jason
       this.users= response.json();
       this.usersKeys = Object.keys(this.users);
       console.log(this.usersKeys)
       console.log(this.users)
    });
   }
 
   deleteUser(key){ //מימוש אופטימיסטיק דליט
     console.log(key);
     let index = this.usersKeys.indexOf(key); //מציאת המקום של הקי שנשלח בפונקציה
     this.usersKeys.splice(index,1); // מחיקת הערך מהמערך
 
   //delete from server
     this.service.deleteUser(key).subscribe(
       response=>console.log(response) // לצורך בדיקה
     );
   }


 

  ngOnInit() {
  	var value = localStorage.getItem('auth');
    


    //////////////////////////////////////////delete these rows to allow entrance to users page without auth
    if(value == 'true'){   
      this.router.navigate(['/users']);
    }else{
      this.router.navigate(['/login']);
    }
  }

}