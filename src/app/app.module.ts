import { SearchResultsComponent } from './products/search-results/search-results.component';

import { ProductsService } from './products/products.service';
import { LoginService } from './login/login.service';
import { UsersService } from './users/users.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import { FormsModule,ReactiveFormsModule} from "@angular/forms";

import { UsersFormComponent } from './users/users-form/users-form.component';
import { UserUpdateComponent } from './users/user-update/user-update.component';
import { NavigatorComponent } from './navigator/navigator.component';
import { UsersComponent } from './users/users.component';
import { NotfoundComponent } from './notfound/notfound.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule} from "angularfire2/database";
import {environment} from './../environments/environment';
import { UsersfireComponent } from './usersfire/usersfire.component';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './users/user/user.component';
import { HomepageComponent } from './homepage/homepage.component';
import { UsersinlineComponent } from './usersinline/usersinline.component';
import { ProductsComponent } from './products/products.component';

import { EditProductComponent } from './products/edit-product/edit-product.component';
import { FproductsComponent } from './fproducts/fproducts.component';





@NgModule({
  declarations: [
    AppComponent,
    NavigatorComponent,
    UsersComponent,
    NotfoundComponent,
    UsersFormComponent,
    UserUpdateComponent,
    UsersfireComponent,
    LoginComponent,
    UserComponent,
    HomepageComponent,
    UsersinlineComponent,
    ProductsComponent,
    EditProductComponent,
    FproductsComponent,
  
    SearchResultsComponent,
  ],
    imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([ // בניה של הראוטס
      {pathMatch:'full',path:'',component:ProductsComponent}, 
      {pathMatch:'full',path:'login',component:LoginComponent},
      {pathMatch:'full',path:'users',component:UsersComponent}, // הראוט הראשון זה דף הבית לוקלהוסט:4200
      {pathMatch:'full',path:'user-update/:id',component:UserUpdateComponent},
       {pathMatch:'full',path:'usersfirebase',component:UsersfireComponent},
       {pathMatch:'full',path:'login',component:LoginComponent},
        {pathMatch:'full',path: 'user/:id', component: UserComponent},
        {pathMatch:'full',path: 'usersinline', component: UsersinlineComponent},
        {pathMatch:'full',path: 'products', component: ProductsComponent},
        {pathMatch:'full',path: 'editproduct/:id', component: EditProductComponent},
        {pathMatch:'full',path: 'fproducts', component: FproductsComponent},
        {pathMatch:'full',path: 'searchresults/:searchvalue', component: SearchResultsComponent},
      //{pathMatch:'full',path:'',},
      
      
     // {path:'message/:id', component:MessageComponent},
      {path:'**',component:NotfoundComponent}// צריך להופיע אחרון כי הוא תופס את כל מה שלא מוגדר ומעביר אליו

    ])
  ],
  providers: [
    UsersService,
    LoginService,
    ProductsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
