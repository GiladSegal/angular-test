import { UsersService } from './../users/users.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-usersfire',
  templateUrl: './usersfire.component.html',
  styleUrls: ['./usersfire.component.css']
})
export class UsersfireComponent implements OnInit {

 users;
  
constructor(private service:UsersService) { }

  ngOnInit() {
    this.service.getUsersFire().subscribe(response=>
    {
      console.log(response);
      this.users=response;
    })
  }

}