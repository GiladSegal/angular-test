import { Component, OnInit,Output , EventEmitter } from '@angular/core';
import { ProductsService } from './../products.service';
import {FormGroup , FormControl} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from "@angular/router";

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  @Output() addProduct:EventEmitter<any> = new EventEmitter<any>(); //any מגדיר סוג מסוים של מידע, במקרה הזה כל סוג
@Output() addProductPs:EventEmitter<any> = new EventEmitter<any>(); //אירוע פסימי

      service:ProductsService;
      //Reactive Form
      //מתאים בדיוק לטופס עצמו
      prdform = new FormGroup({
          name:new FormControl(),
          price:new FormControl()
      });
  constructor(private route: ActivatedRoute ,service: ProductsService, private router:Router) { 
    this.service = service;
  }

    sendData() {
        //הפליטה של העדכון לאב
      this.addProduct.emit(this.prdform.value.name);
      console.log(this.prdform.value);
      this.route.paramMap.subscribe(params=>{
        let id = params.get('id');
        this.service.editProduct(this.prdform.value, id).subscribe(
          response => {
            console.log(response.json());
            this.addProductPs.emit();
            this.router.navigate(['products']);
          }
        );
      })
    }
    product;


  ngOnInit() {

       this.route.paramMap.subscribe(params=>{
           let id = params.get('id');
           console.log(id);
           this.service.getProduct(id).subscribe(response=>{
             this.product = response.json();
             console.log(this.product);
            })
        })
   }
            
}