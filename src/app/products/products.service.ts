import { Injectable } from '@angular/core';
import{Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class ProductsService {
   db:AngularFireDatabase;
  http:Http;//http -> שם התכונה. Http-> סוג התכונה

   getProducts(){
     console.log(environment.url);

    return this.http.get(environment.url+"products");
  }


   editProduct(data,key){
        let options = {
          headers: new Headers({
            'content-type':'application/x-www-form-urlencoded'
          })
   
        }
        let params = new HttpParams().append('name',data.name).append('price',data.price);
        return this.http.put(environment.url+'product/'+ key,params.toString(), options);
      }

 getProduct(id){
    return this.http.get(environment.url+'product/' + id);
  }


  getUsersFire(){
    return this.db.list('/products').valueChanges();
  }

    searchProducts(id){
   
    let options =  {
        headers:new Headers({
          'content-type':'application/x-www-form-urlencoded'     
        })
    } 
       let params = new HttpParams().append('name', id);
    return this.http.post(environment.url + 'products/search', params.toString(), options); 
  }


  



  constructor(http:Http,db:AngularFireDatabase) {
    this.http = http;
    this.db=db;
   }
 
}
