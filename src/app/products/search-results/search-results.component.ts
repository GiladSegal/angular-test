import { ProductsService } from './../products.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Http,Headers } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit {
 
product;

  search_term;
  products;
  productsKeys;
  length_of_keys;// to asses the response from server's length
  service:ProductsService;

  constructor(service:ProductsService, private rout: ActivatedRoute, private router: Router) {
     this.service = service;    
     
     this.rout.paramMap.subscribe(params=>{ //קריאת האיידי מהיואראל

      let id = params.get('searchvalue'); // שליפת האיידי
      console.log(id);
      this.service.searchProducts(id).subscribe(response=>{ // יצירת אובסרבסל
        this.products = response.json();
        console.log(this.products);
         this.productsKeys = Object.keys(this.products);
          this.length_of_keys = this.productsKeys.length;   
      })
    }) 
     
     
     
     
     
  }
  ngOnInit() {
  }

}
