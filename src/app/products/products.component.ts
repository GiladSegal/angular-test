import { ProductsService } from './products.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products;
productsKeys =[];

 searchForm = new FormGroup({
      name:new FormControl()
  }); 



 gotoResults(){
    this.router.navigate(['/searchresults/' + this.searchForm.value.name]);
  }


 constructor(private service:ProductsService,private router:Router,private formBuilder:FormBuilder) {
    service.getProducts().subscribe(
      response=>{//console.log(response)//arrow function .json() converts the string that we recived to jason
      this.products= response.json();
      this.productsKeys = Object.keys(this.products);
      console.log(this.products);
   });

   }

  ngOnInit() {
  }

}




