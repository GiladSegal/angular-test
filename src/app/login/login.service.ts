import { Injectable } from '@angular/core';
import{Http, Headers} from '@angular/http'; 
import {HttpParams} from '@angular/common/http';
import {environment} from './../../environments/environment';

import 'rxjs/Rx';
@Injectable()
export class LoginService {
http:Http;

login(credentials){// זה הסרבר ששולח לשרת את הנתונים של הלוגין לבדיקה
  let options = { // הגדרנו דרך המחלקה של אנגולר האדר , רכיב קונטנט טייפ בישביל שהנתונים יעברו
  headers:new Headers({
   'content-type':'application/x-www-form-urlencoded'
 })
}
let  params = new HttpParams().append('user', credentials.user).append('password',credentials.password); //  מתכוננים לשליחת הנתונים ,פאראמס הוא מבנה נתונים המחזיק מפתח וערך 
return this.http.post(environment.url+"auth", params.toString(),options).map(response=>{ // שליחה לראוט אוט

console.log(response);
 let success = response.json().success;
      if (success == true){
        localStorage.setItem('token','true');
      }else{
       localStorage.setItem('token','false');        
      }
});
}

  constructor(http:Http) { //נוצר אובייקט עם תכונה מסוג היט טי טי פי ובנוסף יש אתחול של תכונה עם דיפנדנסיאיג'קשן שהתכונה של הדיבי תהיה מסוג אנגולר פיירבייס
  this.http = http;
 

}
}