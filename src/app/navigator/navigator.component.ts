import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'navigator',
  templateUrl: './navigator.component.html',
  styleUrls: ['./navigator.component.css']
})
export class NavigatorComponent implements OnInit {

  constructor(private router:Router) { }
  
 logout(){ 
      localStorage.removeItem('token');      
      this.router.navigate(['/login']);
  }

  ngOnInit() {
   
  }

}
