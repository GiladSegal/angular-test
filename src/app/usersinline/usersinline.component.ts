
import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users/users.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-usersinline',
  templateUrl: './usersinline.component.html',
  styleUrls: ['./usersinline.component.css']
})
export class UsersinlineComponent implements OnInit {
  addform = new FormGroup({
    username: new FormControl('',Validators.required),
    email: new FormControl('',Validators.required),
    password: new FormControl
  });

  updateform = new FormGroup({
    username: new FormControl('',Validators.required),
    email: new FormControl('',Validators.required),
    password: new FormControl
  });

  showSlim:Boolean = true; //יוזר המאפשר על ידי לחיצה לראות את הנתונים מסלים כאשר הוא שווה לאמת
  users;
  usersKeys = [];
  updates = [];
  lastOpenedToUpdate;//משתנה המשמש לראות איזה עדכון של יוזר מסוים נפתח אחרון

  sendData(){
    if(this.addform.invalid) return;
    this.service.postUser(this.addform.value).subscribe(response =>{
      console.log(response);
      this.service.getUsers().subscribe(response => {
        this.users =  response.json();
        this.usersKeys = Object.keys(this.users);
      });      
    });
  }

  //פונקציה המגדירה מתי להראות את העדכון
  showUpdate(key){
    if(this.updates[key]){//לסגור את העדכון ליוזר
      this.updates[key] = false;
    }else{
      if(this.lastOpenedToUpdate){ //סוגר את העדכון אם למשתמש אחר פתוח
        this.updates[this.lastOpenedToUpdate] = false;
      }
      //מראה את הטופס למשתמש לאחר שלחץ על עדכון
      this.updates[key] = true;
      this.updateform.get('username').setValue(this.users[key].username);
      this.updateform.get('email').setValue(this.users[key].email);
      this.lastOpenedToUpdate = key;      
    } 
  }

  //עדכון היוזר
  updateUser(id){
    if(this.updateform.invalid) return;
    console.log(this.updateform.value);
    console.log(id);
    this.service.updateUser(id,this.updateform.value).subscribe(response =>{
      console.log(response);
      this.service.getUsers().subscribe(response => {
        this.users =  response.json();
        this.usersKeys = Object.keys(this.users);
      });      
    });    
  }


  constructor(private service:UsersService) {}

  //הצגת רשימת היוזרים
  ngOnInit() {
    this.service.getUsers().subscribe(response => {
      this.users =  response.json();
      this.usersKeys = Object.keys(this.users);
    });
    /*this.service.getMessagesFire().subscribe(fusers =>{
      this.fusers = fusers;
      console.log(this.fusers);
    });    */
  }


}