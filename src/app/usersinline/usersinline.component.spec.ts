import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersinlineComponent } from './usersinline.component';

describe('UsersinlineComponent', () => {
  let component: UsersinlineComponent;
  let fixture: ComponentFixture<UsersinlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersinlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersinlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
