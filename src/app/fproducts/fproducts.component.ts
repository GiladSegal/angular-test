import { ProductsService } from './../products/products.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fproducts',
  templateUrl: './fproducts.component.html',
  styleUrls: ['./fproducts.component.css']
})
export class FproductsComponent implements OnInit {


 products;
  
constructor(private service:ProductsService) { }

  ngOnInit() {
    this.service.getUsersFire().subscribe(response=>
    {
      console.log(response);
      this.products=response;
    })
  }

}