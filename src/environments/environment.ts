// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url: "http://localhost/angular/slim/",
  firebase:{
    apiKey: "AIzaSyDpJEwO12KJqCDfK1wlBFn4E5Fqnt-1kWk",
    authDomain: "exam-facd5.firebaseapp.com",
    databaseURL: "https://exam-facd5.firebaseio.com",
    projectId: "exam-facd5",
    storageBucket: "exam-facd5.appspot.com",
    messagingSenderId: "1077603156116"
  }
};
